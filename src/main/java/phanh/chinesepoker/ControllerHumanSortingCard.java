package phanh.chinesepoker;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class ControllerHumanSortingCard implements Initializable {

    @FXML private Button a_top_1;
    @FXML private Button a_top_2;
    @FXML private Button a_top_3;
    @FXML private Button a_mid_1;
    @FXML private Button a_mid_2;
    @FXML private Button a_mid_3;
    @FXML private Button a_mid_4;
    @FXML private Button a_mid_5;
    @FXML private Button a_bottom_1;
    @FXML private Button a_bottom_2;
    @FXML private Button a_bottom_3;
    @FXML private Button a_bottom_4;
    @FXML private Button a_bottom_5;

    @FXML private Button b_top_1;
    @FXML private Button b_top_2;
    @FXML private Button b_top_3;
    @FXML private Button b_mid_1;
    @FXML private Button b_mid_2;
    @FXML private Button b_mid_3;
    @FXML private Button b_mid_4;
    @FXML private Button b_mid_5;
    @FXML private Button b_bottom_1;
    @FXML private Button b_bottom_2;
    @FXML private Button b_bottom_3;
    @FXML private Button b_bottom_4;
    @FXML private Button b_bottom_5;

    @FXML private Button c_top_1;
    @FXML private Button c_top_2;
    @FXML private Button c_top_3;
    @FXML private Button c_mid_1;
    @FXML private Button c_mid_2;
    @FXML private Button c_mid_3;
    @FXML private Button c_mid_4;
    @FXML private Button c_mid_5;
    @FXML private Button c_bottom_1;
    @FXML private Button c_bottom_2;
    @FXML private Button c_bottom_3;
    @FXML private Button c_bottom_4;
    @FXML private Button c_bottom_5;

    @FXML private Button d_top_1;
    @FXML private Button d_top_2;
    @FXML private Button d_top_3;
    @FXML private Button d_mid_1;
    @FXML private Button d_mid_2;
    @FXML private Button d_mid_3;
    @FXML private Button d_mid_4;
    @FXML private Button d_mid_5;
    @FXML private Button d_bottom_1;
    @FXML private Button d_bottom_2;
    @FXML private Button d_bottom_3;
    @FXML private Button d_bottom_4;
    @FXML private Button d_bottom_5;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        a_top_1.setText("A");

        a_top_2.setText("A");

        a_top_3.setText("A");


        a_mid_1.setText("A");

        a_mid_2.setText("A");

        a_mid_3.setText("A");

        a_mid_4.setText("A");

        a_mid_5.setText("A");

        a_bottom_1.setText("A");

        a_bottom_2.setText("A");

        a_bottom_3.setText("A");

        a_bottom_4.setText("A");

        a_bottom_5.setText("A");


        b_top_1.setText("A");

        b_top_2.setText("A");

        b_top_3.setText("A");


        b_mid_1.setText("A");

        b_mid_2.setText("A");

        b_mid_3.setText("A");

        b_mid_4.setText("A");

        b_mid_5.setText("A");


        b_bottom_1.setText("A");
        b_bottom_2.setText("A");

        b_bottom_3.setText("A");

        b_bottom_4.setText("A");

        b_bottom_5.setText("A");


        c_top_1.setText("A");

        c_top_2.setText("A");

        c_top_3.setText("A");


        c_mid_1.setText("A");

        c_mid_2.setText("A");

        c_mid_3.setText("A");

        c_mid_4.setText("A");

        c_mid_5.setText("A");


        c_bottom_1.setText("A");

        c_bottom_2.setText("A");

        c_bottom_3.setText("A");

        c_bottom_4.setText("A");

        c_bottom_5.setText("A");


        d_top_1.setText("A");

        d_top_2.setText("A");

        d_top_3.setText("A");


        d_mid_1.setText("A");

        d_mid_2.setText("A");

        d_mid_3.setText("A");

        d_mid_4.setText("A");

        d_mid_5.setText("A");


        d_bottom_1.setText("A");

        d_bottom_2.setText("A");

        d_bottom_3.setText("A");

        d_bottom_4.setText("A");
        d_bottom_5.setText("A");

    }
}
