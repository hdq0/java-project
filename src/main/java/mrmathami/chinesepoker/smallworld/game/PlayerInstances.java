package mrmathami.chinesepoker.smallworld.game;

import mrmathami.utilities.enumerate.EnumArray;
import mrmathami.utilities.enumerate.ReadOnlyEnumArray;

public final class PlayerInstances extends ReadOnlyEnumArray<PlayerOrder, PlayerInstance> {
	private PlayerInstances(EnumArray<PlayerOrder, PlayerInstance> enumArray) {
		super(enumArray);
	}

	public static PlayerInstances of(Game game, TableHands tableHands) {
		final EnumArray<PlayerOrder, PlayerInstance> enumArray = new EnumArray<>(PlayerOrder.class, PlayerInstance.class);
		final Players players = game.getPlayers();
		for (final PlayerOrder playerOrder : PlayerOrder.values) {
			enumArray.put(playerOrder, PlayerInstance.of(game, players.get(playerOrder), tableHands, playerOrder));
		}
		return new PlayerInstances(enumArray);
	}
}
