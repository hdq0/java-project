package mrmathami.chinesepoker.smallworld.base;

import mrmathami.utilities.WeakComparable;

public final class HandSet implements WeakComparable<HandSet>, Comparable<HandSet> {
	public static final HandSet EMPTY = new HandSet(null, null, null, HandRoyalty.MIS_SET);
	private final CardSet top;
	private final CardSet mid;
	private final CardSet bottom;
	private final int handRoyalty;

	private HandSet(CardSet top, CardSet mid, CardSet bottom, int handRoyalty) {
		this.top = top;
		this.mid = mid;
		this.bottom = bottom;
		this.handRoyalty = handRoyalty;
	}

	public static HandSet of(CardSet top, CardSet mid, CardSet bottom) {
		assert top.isTrinity() && mid.isQuintet() && bottom.isQuintet();
		return new HandSet(top, mid, bottom, createHandRoyalty(top, mid, bottom));
	}

	public static HandSet of(long top, long mid, long bottom) {
		return of(CardSet.of(top), CardSet.of(mid), CardSet.of(bottom));
	}

	public static HandSet of(int[] top, int[] mid, int[] bottom) {
		return of(CardSet.of(top), CardSet.of(mid), CardSet.of(bottom));
	}

	public static HandSet noWhiteWinOf(HandSet handSet) {
		if (HandRoyalty.isWhiteWin(handSet.getHandRoyalty())) {
			return new HandSet(handSet.top, handSet.mid, handSet.bottom, HandRoyalty.NORMAL);
		}
		return handSet;
	}

	private static int createHandRoyalty(CardSet top, CardSet mid, CardSet bottom) {
		if (top.compareTo(mid) > 0 || mid.compareTo(bottom) > 0)
			return HandRoyalty.MIS_SET;

		final long cards = Cards.of(top.getCards(), mid.getCards(), bottom.getCards());
		if (Cards.countRanks(cards) == 13)
			return HandRoyalty.DRAGON_HAND;

		final int countRed = Cards.countRed(cards);
		if (countRed == 0 || countRed == 13)
			return HandRoyalty.COLOR_THIRTEEN;
		if (countRed == 1 || countRed == 12)
			return HandRoyalty.COLOR_TWELVE;

		if (top.isFlush() && mid.isFlush() && bottom.isFlush())
			return HandRoyalty.THREE_FLUSHES;
		if (top.isStraight() && mid.isStraight() && bottom.isStraight())
			return HandRoyalty.THREE_STRAIGHTS;

		final int[] counters = RankCounters.of(cards);
		if (
				RankCounters.size(counters) == 7 && RankCounters.getCount(counters, 0) == 2
						|| RankCounters.size(counters) == 6 && (RankCounters.getCount(counters, 0) == 3
						|| RankCounters.getCount(counters, 0) == 4) && RankCounters.getCount(counters, 1) == 2
						|| RankCounters.size(counters) == 5 && RankCounters.getCount(counters, 0) == 4
						&& RankCounters.getCount(counters, 1) == 3 && RankCounters.getCount(counters, 2) == 2
		) {
			return HandRoyalty.SIX_PAIRS;
		}

		return HandRoyalty.NORMAL;
	}

	private static int compareTop(CardSet topA, CardSet topB) {
		assert topA.isTrinity() && topB.isTrinity();
		final int topCmp = topA.compareTo(topB);
		if (topCmp > 0) {
			final int royaltyA = topA.getRoyalty();
			final int royaltyB = topB.getRoyalty();
			if (royaltyA == Royalty.THREE_CARDS && royaltyB < Royalty.THREE_CARDS) {
				// Sám chi cuối = 6 cược
				return 3;
			}
			return 1;
		} else if (topCmp < 0) {
			return -compareTop(topB, topA);
		}
		return 0;
	}

	private static int compareMid(CardSet midA, CardSet midB) {
		final int midCmp = midA.compareTo(midB);
		if (midCmp > 0) {
			final int royaltyA = midA.getRoyalty();
			final int royaltyB = midB.getRoyalty();
			if (royaltyA >= Royalty.BABY_STRAIGHT_FLUSH && royaltyB < Royalty.BABY_STRAIGHT_FLUSH) {
				// Thùng phá sảnh chi giữa = 20 cược
				return 10;
			} else if (royaltyA == Royalty.FOUR_CARDS && royaltyB < Royalty.FOUR_CARDS) {
				// Tứ quý chi giữa = 16 cược
				return 8;
			} else if (royaltyA == Royalty.FULL_HOUSE && royaltyB < Royalty.FULL_HOUSE) {
				// Cù lũ chi giữa = 4 cược
				return 2;
			}
			return 1;
		} else if (midCmp < 0) {
			return -compareMid(midB, midA);
		}
		return 0;
	}

	private static int compareBottom(CardSet bottomA, CardSet bottomB) {
		final int bottomCmp = bottomA.compareTo(bottomB);
		if (bottomCmp > 0) {
			final int royaltyA = bottomA.getRoyalty();
			final int royaltyB = bottomB.getRoyalty();
			if (royaltyA >= Royalty.BABY_STRAIGHT_FLUSH && royaltyB < Royalty.BABY_STRAIGHT_FLUSH) {
				// Thùng phá sảnh chi đầu = 10 cược
				return 5;
			} else if (royaltyA == Royalty.FOUR_CARDS && royaltyB < Royalty.FOUR_CARDS) {
				// Tứ quý chi đầu = 8 cược
				return 4;
			}
			return 1;
		} else if (bottomCmp < 0) {
			return -compareBottom(bottomB, bottomA);
		}
		return 0;
	}

	public final int getHandRoyalty() {
		return handRoyalty;
	}

	public CardSet getTop() {
		return top;
	}

	public CardSet getMid() {
		return mid;
	}

	public CardSet getBottom() {
		return bottom;
	}

	@Override
	public Integer weakCompareTo(HandSet handSet) {
		if (handSet == this)
			return 0;

		final int compareTo = Integer.compare(this.getHandRoyalty(), handSet.getHandRoyalty());
		if (compareTo != 0)
			return compareTo;

		final int topCmp = this.top.compareTo(handSet.top);
		final int midCmp = this.mid.compareTo(handSet.mid);
		final int bottomCmp = this.bottom.compareTo(handSet.bottom);

		if (topCmp == 0 && midCmp == 0 && bottomCmp == 0) {
			return 0;
		} else if (topCmp >= 0 && midCmp >= 0 && bottomCmp >= 0) {
			return 1;
		} else if (topCmp <= 0 && midCmp <= 0 && bottomCmp <= 0) {
			return -1;
		}
		return null;
	}

	@Override
	public String toString() {
		return "{" + top + ", " + mid + ", " + bottom + ", " + HandRoyalty.toString(getHandRoyalty()) + "}";
	}

	// TODO: TEST ME: compare and return score (win when bigger than 0)
	@Override
	public int compareTo(HandSet handSet) {
		final int currentHandRoyalty = getHandRoyalty();
		final int opponentHandRoyalty = handSet.getHandRoyalty();
		if (HandRoyalty.isWhiteWin(currentHandRoyalty) && HandRoyalty.isWhiteWin(opponentHandRoyalty)
				|| currentHandRoyalty == HandRoyalty.NORMAL && opponentHandRoyalty == HandRoyalty.NORMAL) {
			final int topCmp = this.top.compareTo(handSet.top);
			final int midCmp = this.mid.compareTo(handSet.mid);
			final int bottomCmp = this.bottom.compareTo(handSet.bottom);

			final int topScore = compareTop(this.top, handSet.top);
			final int midScore = compareMid(this.mid, handSet.mid);
			final int bottomScore = compareBottom(this.bottom, handSet.bottom);

			return (topScore + midScore + bottomScore) << (topCmp == midCmp && midCmp == bottomCmp ? 1 : 0);
		}
		if (currentHandRoyalty > opponentHandRoyalty) {
			if (currentHandRoyalty == HandRoyalty.NORMAL || currentHandRoyalty == HandRoyalty.SIX_PAIRS
					|| currentHandRoyalty == HandRoyalty.THREE_STRAIGHTS || currentHandRoyalty == HandRoyalty.THREE_FLUSHES) {
				return 6;
			} else if (currentHandRoyalty == HandRoyalty.COLOR_TWELVE) {
				return 8;
			} else if (currentHandRoyalty == HandRoyalty.COLOR_THIRTEEN) {
				return 10;
			} else if (currentHandRoyalty == HandRoyalty.DRAGON_HAND) {
				return 24;
			}
		} else {
			if (opponentHandRoyalty == HandRoyalty.NORMAL || opponentHandRoyalty == HandRoyalty.SIX_PAIRS
					|| opponentHandRoyalty == HandRoyalty.THREE_STRAIGHTS || opponentHandRoyalty == HandRoyalty.THREE_FLUSHES) {
				return -6;
			} else if (opponentHandRoyalty == HandRoyalty.COLOR_TWELVE) {
				return -8;
			} else if (opponentHandRoyalty == HandRoyalty.COLOR_THIRTEEN) {
				return -10;
			} else if (opponentHandRoyalty == HandRoyalty.DRAGON_HAND) {
				return -24;
			}
		}
		assert currentHandRoyalty == opponentHandRoyalty && currentHandRoyalty == HandRoyalty.MIS_SET;
		return 0;
	}
}
