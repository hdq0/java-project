package mrmathami.chinesepoker.smallworld.base;

public final class Card {
	public static final int INVALID = -1;
	// region
	public static final int CARD_2C = 0;
	public static final int CARD_2D = 1;
	public static final int CARD_2H = 2;
	public static final int CARD_2S = 3;
	public static final int CARD_3C = 4;
	public static final int CARD_3D = 5;
	public static final int CARD_3H = 6;
	public static final int CARD_3S = 7;
	public static final int CARD_4C = 8;
	public static final int CARD_4D = 9;
	public static final int CARD_4H = 10;
	public static final int CARD_4S = 11;
	public static final int CARD_5C = 12;
	public static final int CARD_5D = 13;
	public static final int CARD_5H = 14;
	public static final int CARD_5S = 15;
	public static final int CARD_6C = 16;
	public static final int CARD_6D = 17;
	public static final int CARD_6H = 18;
	public static final int CARD_6S = 19;
	public static final int CARD_7C = 20;
	public static final int CARD_7D = 21;
	public static final int CARD_7H = 22;
	public static final int CARD_7S = 23;
	public static final int CARD_8C = 24;
	public static final int CARD_8D = 25;
	public static final int CARD_8H = 26;
	public static final int CARD_8S = 27;
	public static final int CARD_9C = 28;
	public static final int CARD_9D = 29;
	public static final int CARD_9H = 30;
	public static final int CARD_9S = 31;
	public static final int CARD_10C = 32;
	public static final int CARD_10D = 33;
	public static final int CARD_10H = 34;
	public static final int CARD_10S = 35;
	public static final int CARD_JC = 36;
	public static final int CARD_JD = 37;
	public static final int CARD_JH = 38;
	public static final int CARD_JS = 39;
	public static final int CARD_QC = 40;
	public static final int CARD_QD = 41;
	public static final int CARD_QH = 42;
	public static final int CARD_QS = 43;
	public static final int CARD_KC = 44;
	public static final int CARD_KD = 45;
	public static final int CARD_KH = 46;
	public static final int CARD_KS = 47;
	public static final int CARD_AC = 48;
	public static final int CARD_AD = 49;
	public static final int CARD_AH = 50;
	public static final int CARD_AS = 51;
	// endregion
	public static final int LENGTH = 52;

	private static final int[] values = {
			0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
			26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51
	};

	private static final String[] CARD_NAME_TABLE = {
			"2♣", "2♦", "2♥", "2♠", "3♣", "3♦", "3♥", "3♠", "4♣", "4♦", "4♥", "4♠", "5♣", "5♦", "5♥", "5♠",
			"6♣", "6♦", "6♥", "6♠", "7♣", "7♦", "7♥", "7♠", "8♣", "8♦", "8♥", "8♠", "9♣", "9♦", "9♥", "9♠",
			"10♣", "10♦", "10♥", "10♠", "J♣", "J♦", "J♥", "J♠", "Q♣", "Q♦", "Q♥", "Q♠", "K♣", "K♦", "K♥", "K♠",
			"A♣", "A♦", "A♥", "A♠"
	};

	private Card() {
	}

	public static int[] values() {
		return values.clone();
	}

	public static boolean isValid(int cardNum) {
		return cardNum >= 0 && cardNum < LENGTH;
	}

	public static String toString(int cardNum) {
		// should be an assertion, this function is internal use only
		assert isValid(cardNum) : "cardNum out of range! cardNum = " + cardNum;
		return CARD_NAME_TABLE[cardNum];
	}

	public static int getRank(int cardNum) {
		// should be an assertion, this function is internal use only
		assert isValid(cardNum) : "cardNum out of range! cardNum = " + cardNum;
		return cardNum >> 2;
	}

	public static int getSuit(int cardNum) {
		// should be an assertion, this function is internal use only
		assert isValid(cardNum) : "cardNum out of range! cardNum = " + cardNum;
		return cardNum & 3;
	}
}
