package mrmathami.chinesepoker.world.base;

public enum Royalty {
	HIGH_CARDS("High cards"),
	ONE_PAIR("One pair"),
	TWO_PAIRS("Two pairs"),
	THREE_CARDS("Three of a kind"),
	BABY_STRAIGHT("Baby straight"),
	STRAIGHT("Straight"),
	FLUSH("Flush"),
	FULL_HOUSE("Full house"),
	FOUR_CARDS("Four of a kind"),
	BABY_STRAIGHT_FLUSH("Baby straight flush"),
	STRAIGHT_FLUSH("Straight flush");

	public static final Royalty[] values = Royalty.values();
	private final String string;

	Royalty(String string) {
		this.string = string;
	}

	@Override
	public String toString() {
		return this.string;
	}
}
