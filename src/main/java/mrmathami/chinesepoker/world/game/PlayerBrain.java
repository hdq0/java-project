package mrmathami.chinesepoker.world.game;

import mrmathami.utilities.Timer;

@FunctionalInterface
public interface PlayerBrain {
	/**
	 * For you to fight.
	 * This method runs in an independent thread.
	 * Check the timer and the interrupt to exit on time.
	 *
	 * @param tableHands       tableHands
	 * @param playerOrder playerOrder
	 * @param timer       timer
	 * @param result      result
	 */
	void play(TableHands tableHands, PlayerOrder playerOrder, Timer timer, PlayerResult result);
}
