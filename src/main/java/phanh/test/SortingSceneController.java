package phanh.test;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

import java.net.URL;
import java.util.ResourceBundle;

public final class SortingSceneController implements Initializable {
	@FXML public GridPane firstHand;
	@FXML public GridPane secondHand;
	@FXML public GridPane thirdHand;
	@FXML public GridPane mainHand;
	@FXML public HandSetFragmentController firstHandController;
	@FXML public HandSetFragmentController secondHandController;
	@FXML public HandSetFragmentController thirdHandController;
	@FXML public HandSetFragmentController mainHandController;

	@FXML public Label countDown;
	@FXML public Button finish;


	private GUIController guiController;

	public GUIController getGuiController() {
		return guiController;
	}

	public void setGuiController(GUIController guiController) {
		this.guiController = guiController;
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		mainHandController.setSwappable(true);
	}

	public void onFinishAction(ActionEvent event) {
		guiController.onFinishAction();
	}
}


