package mrmathami.chinesepoker.smallworld.computer;

import mrmathami.chinesepoker.smallworld.base.Card;
import mrmathami.chinesepoker.smallworld.base.Cards;
import mrmathami.chinesepoker.smallworld.base.HandRoyalty;
import mrmathami.chinesepoker.smallworld.base.HandSet;

import java.security.SecureRandom;
import java.util.*;

public final class SingleSolver {
	private SingleSolver() {
	}

	public static void main(String[] args) throws ClassNotFoundException {
		Random randomSource = new SecureRandom(SecureRandom.getSeed(256));
		final int[] deck = Card.values();
		final int[][] hands = new int[4][13];
		{
			int length = deck.length - 1;
			while (length >= 0) {
				final int random = randomSource.nextInt(length + 1);
				final int card = deck[random];
				deck[random] = deck[length];
				deck[length] = card;
				length -= 1;
			}
		}
		int length = deck.length - 1;
		while (length >= 0) {
			final int random = randomSource.nextInt(length + 1);
			hands[length & 3][length >> 2] = deck[random];
			deck[random] = deck[length--];
		}

		Class.forName("mrmathami.chinesepoker.smallworld.base.CardSet");

		long startTime = System.nanoTime();
		System.out.println(solve(Cards.of(hands[0]), Cards.of(hands[1])));
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println((double) totalTime / 1000000.0);
	}

	public static HandSet solve(long myHand, long opponentHand) {
		assert Cards.size(myHand) == 13 && Cards.size(opponentHand) == 13;
		final List<HandSet> myHandSets = HandSetListBuilder.build(myHand);
		final List<HandSet> opponentHandSets = HandSetListBuilder.build(opponentHand);

		HandSet myBestHandSet = null;

		int myBestResultSum = Integer.MIN_VALUE;
		int myBestResultMin = Integer.MIN_VALUE;
		int myBestWinCount = Integer.MIN_VALUE;
		int myBestWinSum = Integer.MIN_VALUE;
		int myBestDrawCount = Integer.MIN_VALUE;

		for (HandSet myHandSet : myHandSets) {
			int myResultSum = 0;
			int myResultMin = Integer.MAX_VALUE;
			int myWinCount = 0;
			int myWinSum = 0;
			int myDrawCount = 0;

			for (HandSet opponentHandSet : opponentHandSets) {
				int result = myHandSet.compareTo(opponentHandSet);
				myResultSum += result;
				if (myResultMin > result) myResultMin = result;
				if (result == 0) {
					myDrawCount += 1;
				} else if (result > 0) {
					myWinCount += 1;
					myWinSum += result;
				}
			}

//			System.out.println("{");
//			System.out.println("\tmyHandSet = " + myHandSet);
//			System.out.println("\tmyResultSum = " + myResultSum);
//			System.out.println("\tmyResultMin = " + myResultMin);
//			System.out.println("\tmyWinCount = " + myWinCount);
//			System.out.println("\tmyWinSum = " + myWinSum);
//			System.out.println("\tmyDrawCount = " + myDrawCount);
//			System.out.println("\tmyBestHandSet = " + myBestHandSet);
//			System.out.println("\tmyBestResultSum = " + myBestResultSum);
//			System.out.println("\tmyBestResultMin = " + myBestResultMin);
//			System.out.println("\tmyBestWinCount = " + myBestWinCount);
//			System.out.println("\tmyBestWinSum = " + myBestWinSum);
//			System.out.println("\tmyBestDrawCount = " + myBestDrawCount);
//			System.out.println("}");

			if (
					myWinCount > myBestWinCount || myWinCount == myBestWinCount && (
							myDrawCount > myBestDrawCount || myDrawCount == myBestDrawCount && (
									myResultMin > myBestResultMin || myResultMin == myBestResultMin && (
											myWinSum > myBestWinSum || myWinSum == myBestWinSum && (
													myResultSum > myBestResultSum
											)
									)
							)
					)
			) {
				myBestHandSet = myHandSet;
				myBestResultSum = myResultSum;
				myBestResultMin = myResultMin;
				myBestWinCount = myWinCount;
				myBestWinSum = myWinSum;
				myBestDrawCount = myDrawCount;
			}
		}

		return myBestHandSet;
	}


	private static final class HandSetListBuilder {
		private final int[] cardArray;
		private final List<HandSet> handSets;
		private final int[] top = new int[3];
		private final int[] mid = new int[5];
		private final int[] bottom = new int[5];

		private HandSetListBuilder(long cards) {
			this.cardArray = Cards.toArray(cards);
			this.handSets = new LinkedList<>();
		}

		private static List<HandSet> build(long cards) {
			HandSetListBuilder builder = new HandSetListBuilder(cards);
			builder.recursiveCall(0, 0, 0, 0);
			return new ArrayList<>(builder.handSets);
		}

		private static boolean shouldAdd(List<HandSet> handSets, HandSet myHandSet) {
			boolean shouldAdd = true;
			for (Iterator<HandSet> iterator = handSets.iterator(); iterator.hasNext(); ) {
				HandSet handSet = iterator.next();
				Integer weakCompareTo = myHandSet.weakCompareTo(handSet);
				if (weakCompareTo != null) {
					if (weakCompareTo < 0) {
						shouldAdd = false;
						break;
					} else if (weakCompareTo == 0) {
						shouldAdd = false;
					} else {
						iterator.remove();
					}
				}
			}
			return shouldAdd;
		}

		private void recursiveCall(int cardIndex, int topSize, int midSize, int bottomSize) {
			if (cardIndex >= 13) {
				if (topSize == 3 && midSize == 5 && bottomSize == 5) {
					final HandSet myHandSet = HandSet.of(top, mid, bottom);
					if (shouldAdd(handSets, myHandSet)) {
						handSets.add(myHandSet);
					}
					if (HandRoyalty.isWhiteWin(myHandSet.getHandRoyalty())) {
						final HandSet myLowHandSet = HandSet.noWhiteWinOf(myHandSet);
						if (shouldAdd(handSets, myLowHandSet)) {
							handSets.add(myLowHandSet);
						}
					}
				}
			} else {
				if (topSize < 3) {
					top[topSize] = cardArray[cardIndex];
					recursiveCall(cardIndex + 1, topSize + 1, midSize, bottomSize);
				}
				if (midSize < 5) {
					mid[midSize] = cardArray[cardIndex];
					recursiveCall(cardIndex + 1, topSize, midSize + 1, bottomSize);
				}
				if (bottomSize < 5) {
					bottom[bottomSize] = cardArray[cardIndex];
					recursiveCall(cardIndex + 1, topSize, midSize, bottomSize + 1);
				}
			}
		}
	}

}
