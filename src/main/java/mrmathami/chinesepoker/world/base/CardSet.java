package mrmathami.chinesepoker.world.base;

import java.util.ArrayList;

public final class CardSet implements Comparable<CardSet> {
	private static final CardSet[] CARD_SETS = createCardSets();
	private final Cards cards;
	private final Royalty royalty;
	private final boolean isStraight;
	private final int score;

	private CardSet(Cards cards, Royalty royalty, boolean isStraight, int score) {
		this.cards = cards;
		this.royalty = royalty;
		this.isStraight = isStraight;
		this.score = score;
	}

	public static CardSet of(Cards cards) {
		assert cards.size() == 3 || cards.size() == 5;
		return getByPrimitive(cards.toPrimitive());
	}

	public static CardSet of(Card... cards) {
		assert cards.length == 3 || cards.length == 5;
		return getByPrimitive(Cards.toPrimitive(cards));
	}

	private static CardSet[] createCardSets() {
		final ArrayList<InternalCardSet> list = new ArrayList<>();
		final Card[] values = Card.values;
		for (int a = 0; a < values.length; a++) {
			for (int b = a + 1; b < values.length; b++) {
				for (int c = b + 1; c < values.length; c++) {
					// add Trinity
					list.add(InternalCardSet.of(values[a], values[b], values[c]));

					for (int d = c + 1; d < values.length; d++) {
						for (int e = d + 1; e < values.length; e++) {
							// add CardSet
							list.add(InternalCardSet.of(values[a], values[b], values[c], values[d], values[e]));
						}
					}
				}
			}
		}
		list.sort(null);

		final ArrayList<CardSet> cardSets = new ArrayList<>();

		int cardScore = 1;
		InternalCardSet lastInternalCardSet = null;
		for (InternalCardSet cardSet : list) {
			if (lastInternalCardSet != null && cardSet.compareTo(lastInternalCardSet) != 0) cardScore++;
			lastInternalCardSet = cardSet;
			cardSets.add(new CardSet(cardSet.cards, cardSet.royalty, cardSet.isStraight, cardScore));
		}
		cardSets.sort(CardSet::compareByPrimitive);
		return cardSets.toArray(new CardSet[0]);
	}

	private static CardSet getByPrimitive(long primitive) {
		int low = 0;
		int high = CARD_SETS.length - 1;

		while (low <= high) {
			final int mid = (low + high) >> 1;
			final CardSet cardSet = CARD_SETS[mid];
			final int cmp = Long.compare(primitive, cardSet.cards.toPrimitive());
			if (cmp > 0) {
				low = mid + 1;
			} else if (cmp < 0) {
				high = mid - 1;
			} else {
				return cardSet;
			}
		}
		return null;
	}

	private static int compareByPrimitive(CardSet cardSetA, CardSet cardSetB) {
		return Long.compare(cardSetA.cards.toPrimitive(), cardSetB.cards.toPrimitive());
	}

	public Cards getCards() {
		return this.cards;
	}

	public Royalty getRoyalty() {
		return this.royalty;
	}

	public boolean isQuintet() {
		return this.cards.size() == 5;
	}

	public boolean isTrinity() {
		return this.cards.size() == 3;
	}

	public boolean isStraight() {
		return this.isStraight;
	}

	public boolean isFlush() {
		return this.cards.isSameSuit();
	}

	@Override
	public boolean equals(Object object) {
		return object == this || object instanceof CardSet && cards.equals(((CardSet) object).cards);
	}

	@Override
	public int hashCode() {
		return cards.hashCode();
	}

	@Override
	public int compareTo(CardSet cardSet) {
		return Integer.compare(this.score, cardSet.score);
	}

	@Override
	public String toString() {
		return "{" + cards + ", " + royalty + ", " + score + "}";
	}

	private static final class InternalCardSet implements Comparable<InternalCardSet> {
		private final Cards cards;
		private final Royalty royalty;
		private final boolean isStraight;
		private final RankCounters rankCounters;

		private InternalCardSet(Cards cards, Royalty royalty, boolean isStraight, RankCounters rankCounters) {
			this.cards = cards;
			this.royalty = royalty;
			this.isStraight = isStraight;
			this.rankCounters = rankCounters;
		}

		static InternalCardSet of(Card... cardArray) {
			assert cardArray.length == 3 || cardArray.length == 5;

			// createCardSets cards
			final Cards cards = Cards.of(cardArray);

			// createCardSets descending sorted rankCounters
			final RankCounters rankCounters = RankCounters.of(cards);

			// createCardSets royalty
			Royalty royalty;
			boolean isStraight = false;

			if (cards.size() == 5) {
				if (rankCounters.size() == 5) {
					final boolean isBabyStraight = (rankCounters.getRankOrdinal(0) == Rank.RANK_A.ordinal() && rankCounters.getRankOrdinal(1) == Rank.RANK_5.ordinal());
					final boolean isNormalStraight = rankCounters.getRankOrdinal(0) - rankCounters.getRankOrdinal(4) == 4;
					isStraight = isBabyStraight || isNormalStraight;
					royalty = cards.isSameSuit() ?
							(isNormalStraight ? Royalty.STRAIGHT_FLUSH :
									(isBabyStraight ? Royalty.BABY_STRAIGHT_FLUSH : Royalty.FLUSH)) :
							(isNormalStraight ? Royalty.STRAIGHT :
									(isBabyStraight ? Royalty.BABY_STRAIGHT : Royalty.HIGH_CARDS));

				} else if (rankCounters.size() == 4) {
					royalty = Royalty.ONE_PAIR;
				} else if (rankCounters.size() == 3) {
					royalty = rankCounters.getCount(0) == 3 ? Royalty.THREE_CARDS : Royalty.TWO_PAIRS;
				} else {
					royalty = rankCounters.getCount(0) == 4 ? Royalty.FOUR_CARDS : Royalty.FULL_HOUSE;
				}
			} else {
				if (rankCounters.size() == 3) {
					final boolean isBabyStraight = (rankCounters.getRankOrdinal(0) == Rank.RANK_A.ordinal() && rankCounters.getRankOrdinal(1) == Rank.RANK_3.ordinal());
					final boolean isNormalStraight = rankCounters.getRankOrdinal(0) - rankCounters.getRankOrdinal(2) == 2;
					isStraight = isBabyStraight || isNormalStraight;
					royalty = Royalty.HIGH_CARDS;
				} else {
					royalty = rankCounters.size() == 2 ? Royalty.ONE_PAIR : Royalty.THREE_CARDS;
				}
			}

			return new InternalCardSet(cards, royalty, isStraight, rankCounters);
		}

		@Override
		public final int compareTo(InternalCardSet internalCardSet) {
			// same card set check
			if (this.cards.equals(internalCardSet.cards)) return 0;

			// compare royalty
			final int compareTo = this.royalty.compareTo(internalCardSet.royalty);
			if (compareTo != 0) return compareTo;

			// specific to CountedQuintet
			switch (this.royalty) {
				case BABY_STRAIGHT:
				case BABY_STRAIGHT_FLUSH:
					return 0;
				case STRAIGHT:
				case STRAIGHT_FLUSH:
					return Integer.compare(this.rankCounters.getRankOrdinal(0), internalCardSet.rankCounters.getRankOrdinal(0));
			}

			// compare counters
			return this.rankCounters.compareTo(internalCardSet.rankCounters);
		}
	}
}
