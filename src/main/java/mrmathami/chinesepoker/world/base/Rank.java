package mrmathami.chinesepoker.world.base;

public enum Rank {
	RANK_2("2"),
	RANK_3("3"),
	RANK_4("4"),
	RANK_5("5"),
	RANK_6("6"),
	RANK_7("7"),
	RANK_8("8"),
	RANK_9("9"),
	RANK_10("10"),
	RANK_J("J"),
	RANK_Q("Q"),
	RANK_K("K"),
	RANK_A("A");

	public static final Rank[] values = Rank.values();
	private final String string;

	Rank(String string) {
		this.string = string;
	}

	@Override
	public String toString() {
		return this.string;
	}
}
