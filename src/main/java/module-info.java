module hello {
	opens mrmathami.chinesepoker.gui;

	requires javafx.base;
	requires javafx.graphics;
	requires javafx.controls;
	requires javafx.fxml;
}