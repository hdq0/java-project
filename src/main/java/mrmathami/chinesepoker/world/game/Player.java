package mrmathami.chinesepoker.world.game;

/**
 *
 */
public final class Player {
	private final PlayerBrain playerBrain;
	private long score;

	private Player(PlayerBrain playerBrain, long score) {
		this.playerBrain = playerBrain;
		this.score = score;
	}

	public static Player of(PlayerBrain playerBrain) {
		return new Player(playerBrain, 0);
	}

	public static Player of(PlayerBrain playerBrain, long score) {
		return new Player(playerBrain, score);
	}

	public PlayerBrain getPlayerBrain() {
		return playerBrain;
	}

	public long getScore() {
		return score;
	}

	public long modifyScore(long deltaScore) {
		score += deltaScore;
		return score;
	}
}
