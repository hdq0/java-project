package mrmathami.utilities;


/**
 * The interface implies that this Class can be weakly compare. For "example",
 * a Intel Core i5 Gen 8 is better than a Intel Core i3 Gen 7, but with a Intel
 * Core i7 Gen 6, it is unclear who is the winner (in term of performance).
 *
 * @param <T>
 */
@FunctionalInterface
public interface WeakComparable<T> {
	/**
	 * result >  0    means this is clearly better
	 * result <  0    means this is clearly worse
	 * result == 0    means this is clearly equal
	 * result == null means this is unclear whether it is better or worse
	 *
	 * @param o object to compare
	 * @return result
	 */
	Integer weakCompareTo(T o);
}
