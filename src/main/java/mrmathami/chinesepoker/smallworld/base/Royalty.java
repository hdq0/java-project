package mrmathami.chinesepoker.smallworld.base;

public final class Royalty {
	public static final int INVALID = -1;
	// region
	public static final int HIGH_CARDS = 0;
	public static final int ONE_PAIR = 1;
	public static final int TWO_PAIRS = 2;
	public static final int THREE_CARDS = 3;
	public static final int BABY_STRAIGHT = 4;
	public static final int STRAIGHT = 5;
	public static final int FLUSH = 6;
	public static final int FULL_HOUSE = 7;
	public static final int FOUR_CARDS = 8;
	public static final int BABY_STRAIGHT_FLUSH = 9;
	public static final int STRAIGHT_FLUSH = 10;
	// endregion
	public static final int LENGTH = 11;

	public static final int[] values = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	private static final String[] ROYALTY_STRING_TABLE = {
			"High cards", "One pair", "Two pairs", "Three of a kind",
			"Baby straight", "Straight", "Flush", "Full house",
			"Four of a kind", "Baby straight flush", "Straight flush"
	};

	private Royalty() {
	}

	public static int[] values() {
		return values.clone();
	}

	public static boolean isValid(int royaltyNum) {
		return royaltyNum >= 0 && royaltyNum < LENGTH;
	}

	public static String toString(int royaltyNum) {
		// should be an assertion, this function is internal use only
		assert isValid(royaltyNum) : "royaltyNum out of range! royaltyNum = " + royaltyNum;
		return ROYALTY_STRING_TABLE[royaltyNum];
	}
}
