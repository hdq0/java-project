package mrmathami.chinesepoker.world.base;

public class Test {
	public static void main(String[] args) throws ClassNotFoundException, InterruptedException {
		// write your code here
		long startTime = System.nanoTime();

		Class.forName("mrmathami.chinesepoker.world.base.CardSet");
		long endTime = System.nanoTime();
		long totalTime = endTime - startTime;
		System.out.println((double) totalTime / 1000000.0);
//		for (CardSet cardSet : CardSet.CARD_SETS) {
//			System.out.println(cardSet);
//		}

		Thread.sleep(10000);
	}

}
