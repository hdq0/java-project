package mrmathami.chinesepoker.world.game;

import mrmathami.chinesepoker.world.base.HandSet;
import mrmathami.utilities.enumerate.EnumArray;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public final class GameInstance {
	private final Game game;
	private final TableHands tableHands;
	private final PlayerInstances playerInstances;
	private final Runnable onStartCallback;
	private final Runnable onTickCallback;
	private final Runnable onFinishCallback;
	private TableHandSets handSets;
	private TableDeltas deltas;
	private ScheduledFuture<?> scheduledFuture;

	private GameInstance(Game game, Runnable onStartCallback, Runnable onTickCallback, Runnable onFinishCallback) {
		this.game = game;
		this.tableHands = TableHands.generateTableHands(game.getRandomSource());
		this.playerInstances = PlayerInstances.of(game, this.tableHands);
		this.onStartCallback = onStartCallback;
		this.onTickCallback = onTickCallback;
		this.onFinishCallback = onFinishCallback;
	}

	public static GameInstance of(Game game, Runnable onStartCallback, Runnable onTickCallback, Runnable onFinishCallback) {
		return new GameInstance(game, onStartCallback, onTickCallback, onFinishCallback);
	}

	public synchronized void start() {
		if (scheduledFuture != null) return;
		for (PlayerInstance playerInstance : playerInstances) playerInstance.start();
		if (onStartCallback != null) onStartCallback.run();
		this.scheduledFuture = game.getScheduledExecutor()
				.scheduleAtFixedRate(this::run, 1, 1, TimeUnit.SECONDS);
	}

	public synchronized void stop() {
		if (scheduledFuture == null || scheduledFuture.isDone()) return;
		for (PlayerInstance playerInstance : playerInstances) playerInstance.stop();
		waitUntilFinish();
	}

	public void waitUntilFinish() {
		if (!isStarted()) return;
		try {
			getScheduledFuture().get();
		} catch (Exception ignored) {
		}
	}

	private synchronized void onFinish() {
		final EnumArray<PlayerOrder, HandSet> handSets = new EnumArray<>(PlayerOrder.class, HandSet.class);
		for (final PlayerOrder playerOrder : PlayerOrder.values) {
			handSets.put(playerOrder, playerInstances.get(playerOrder).getResult());
		}
		this.handSets = TableHandSets.of(handSets);

		final EnumArray<PlayerOrder, Long> deltas = new EnumArray<>(PlayerOrder.class, Long.class);
		final Players players = game.getPlayers();
		for (final PlayerOrder myPlayerOrder : PlayerOrder.values) {
			final HandSet myHandSet = handSets.get(myPlayerOrder);
			long compareHand = 0;
			for (final PlayerOrder opponentPlayerOrder : PlayerOrder.values) {
				final HandSet opponentHandSet = handSets.get(opponentPlayerOrder);
				if (myHandSet != opponentHandSet) {
					compareHand += myHandSet.compareHand(opponentHandSet);
				}
			}
			deltas.put(myPlayerOrder, compareHand);
			players.get(myPlayerOrder).modifyScore(compareHand);
		}
		this.deltas = TableDeltas.of(deltas);

		if (onFinishCallback != null) onFinishCallback.run();
	}

	private synchronized void onTick() {
		if (onTickCallback != null) onTickCallback.run();
	}

	private synchronized void run() {
		if (!isPlayerInstancesFinished()) {
			onTick();
		} else {
			scheduledFuture.cancel(false);
			onFinish();
		}
	}

	private boolean isPlayerInstancesStarted() {
		for (PlayerInstance playerInstance : playerInstances) {
			if (!playerInstance.isStarted()) return false;
		}
		return true;
	}

	private boolean isPlayerInstancesFinished() {
		for (PlayerInstance playerInstance : playerInstances) {
			if (!playerInstance.isFinished()) return false;
		}
		return true;
	}

	private synchronized ScheduledFuture<?> getScheduledFuture() {
		return scheduledFuture;
	}

	public synchronized boolean isStarted() {
		return scheduledFuture != null;
	}

	public synchronized boolean isFinished() {
		return scheduledFuture != null && scheduledFuture.isDone();
	}

	public synchronized TableHandSets getHandSets() {
		return isFinished() ? handSets : null;
	}

	public synchronized TableDeltas getDeltas() {
		return isFinished() ? deltas : null;
	}

	public TableHands getTableHands() {
		return tableHands;
	}

	public PlayerInstances getPlayerInstances() {
		return playerInstances;
	}
}
