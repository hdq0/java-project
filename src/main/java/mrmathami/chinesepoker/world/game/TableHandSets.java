package mrmathami.chinesepoker.world.game;

import mrmathami.chinesepoker.world.base.HandSet;
import mrmathami.utilities.enumerate.EnumArray;
import mrmathami.utilities.enumerate.ReadOnlyEnumArray;

public final class TableHandSets extends ReadOnlyEnumArray<PlayerOrder, HandSet> {
	private TableHandSets(EnumArray<PlayerOrder, HandSet> enumArray) {
		super(enumArray);
	}

	public static TableHandSets of(EnumArray<PlayerOrder, HandSet> enumArray) {
		return new TableHandSets(enumArray);
	}
}