package mrmathami.utilities.enumerate;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;

public class EnumArray<EnumType extends Enum<EnumType>, ValueType> implements Iterable<ValueType> {
	private final ValueType[] values;

	public EnumArray(Class<EnumType> enumClass, Class<ValueType> valueClass) {
		@SuppressWarnings("unchecked") final ValueType[] values = (ValueType[]) Array.newInstance(valueClass, enumClass.getEnumConstants().length);
		this.values = values;
	}

	public EnumArray(EnumArray<EnumType, ValueType> enumArray) {
		this.values = enumArray.values.clone();
	}

	public ValueType get(EnumType key) {
		return values[key.ordinal()];
	}

	public void put(EnumType key, ValueType value) {
		values[key.ordinal()] = value;
	}

	@Override
	public Iterator<ValueType> iterator() {
		return new EnumArrayIterator<>(this);
	}

	@Override
	public String toString() {
		return Arrays.toString(values);
	}

	private static final class EnumArrayIterator<EnumType extends Enum<EnumType>, ValueType> implements Iterator<ValueType> {
		private final EnumArray<EnumType, ValueType> array;
		private int index = 0;

		private EnumArrayIterator(EnumArray<EnumType, ValueType> array) {
			this.array = array;
		}

		@Override
		public boolean hasNext() {
			return index < array.values.length;
		}

		@Override
		public ValueType next() {
			return array.values[index++];
		}
	}
}
