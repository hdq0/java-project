package mrmathami.chinesepoker.smallworld.computer;

import mrmathami.chinesepoker.smallworld.base.HandSet;
import mrmathami.chinesepoker.smallworld.game.PlayerOrder;
import mrmathami.chinesepoker.smallworld.game.TableHands;
import mrmathami.utilities.ThreadFactoryBuilder;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

final class TableHandSetArray {
	private static final ExecutorService EXECUTOR_SERVICE = Executors.newCachedThreadPool(
			new ThreadFactoryBuilder()
					.setNamePrefix("TableHandSetArrayExecutorService")
					.setDaemon(true)
					.setPriority(Thread.MAX_PRIORITY)
					.build()
	);

	private final TableHands tableHands;
	private final EnumMap<PlayerOrder, HandSet[]> playerHandSetArray;

	private TableHandSetArray(TableHands tableHands, EnumMap<PlayerOrder, HandSet[]> playerHandSetArray) {
		this.tableHands = tableHands;
		this.playerHandSetArray = playerHandSetArray;
	}

	static TableHandSetArray of(TableHands tableHands) throws InterruptedException {
		final EnumMap<PlayerOrder, HandSet[]> playerHandSets = new EnumMap<>(PlayerOrder.class);

		List<PlayerHandSetArray> list = new ArrayList<>(PlayerOrder.values.length);
		for (final PlayerOrder playerOrder : PlayerOrder.values) list.add(PlayerHandSetArray.of(tableHands, playerOrder));
		List<Future<PlayerHandSetArray>> futures = EXECUTOR_SERVICE.invokeAll(list);

		try {
			for (final Future<PlayerHandSetArray> future : futures) {
				final PlayerHandSetArray playerHandSetArray = future.get();
				playerHandSets.put(playerHandSetArray.getPlayerOrder(), playerHandSetArray.getHandSetArray());
			}
		} catch (ExecutionException e) {
			throw new InterruptedException();
		}
		return new TableHandSetArray(tableHands, playerHandSets);
	}

	TableHands getTableHands() {
		return tableHands;
	}

	HandSet[] getPlayerHandSetArray(PlayerOrder playerOrder) {
		return playerHandSetArray.get(playerOrder);
	}
}


