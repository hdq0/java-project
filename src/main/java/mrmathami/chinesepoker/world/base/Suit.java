package mrmathami.chinesepoker.world.base;

public enum Suit {
	SUIT_CLUB("♣"),
	SUIT_DIAMOND("♦"),
	SUIT_HEART("♥"),
	SUIT_SPADE("♠");

	public static final Suit[] values = Suit.values();
	private final String string;

	Suit(String string) {
		this.string = string;
	}

	public boolean isRed() {
		return this == SUIT_DIAMOND || this == SUIT_HEART;
	}

	public boolean isBlack() {
		return this == SUIT_CLUB || this == SUIT_SPADE;
	}

	@Override
	public String toString() {
		return this.string;
	}
}
