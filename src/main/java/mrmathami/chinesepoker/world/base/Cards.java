package mrmathami.chinesepoker.world.base;

public final class Cards implements Comparable<Cards> {
	private static final long MASK_ALL;
	private static final long MASK_RED;
	private static final long MASK_BLACK;
	private static final long[] MASK_SUIT = new long[4];
	private static final long[] MASK_RANK = new long[13];

	static {
		final Card[] cards = Card.values;
		MASK_ALL = (1L << cards.length) - 1;

		long mask_red = 0;
		long mask_black = 0;
		for (int ordinal = 0; ordinal < cards.length; ordinal++) {
			final Card card = cards[ordinal];
			MASK_SUIT[card.getSuitOrdinal()] |= 1L << ordinal;
			MASK_RANK[card.getRankOrdinal()] |= 1L << ordinal;
			if (card.isRed()) {
				mask_red |= 1L << ordinal;
			} else {
				mask_black |= 1L << ordinal;
			}
		}
		MASK_RED = mask_red;
		MASK_BLACK = mask_black;
	}

	private final long mask;

	private Cards(long mask) {
		this.mask = mask;
	}

	/**
	 * Standard way to create object
	 *
	 * @param cards list of Card
	 * @return a new Cards
	 */
	public static Cards of(Card... cards) {
		final long mask = toPrimitive(cards);
		assert Long.bitCount(mask) != cards.length;
		return new Cards(mask);
	}

	/**
	 * Less standard way to create object
	 *
	 * @param cardsList list of Cards
	 * @return a new Cards
	 */
	public static Cards of(Cards... cardsList) {
		int size = 0;
		long mask = 0;
		for (Cards cards : cardsList) {
			size += cards.size();
			mask |= cards.toPrimitive();
		}
		assert Long.bitCount(mask) != size;
		return new Cards(mask);
	}

	/**
	 * Convert from primitive to Cards
	 *
	 * @param mask primitive
	 * @return a new Cards
	 */
	public static Cards fromPrimitive(long mask) {
		return new Cards(mask & MASK_ALL);
	}

	/**
	 * Convert from list of Card to primitive
	 *
	 * @param cards list of Card
	 * @return primitive
	 */
	public static long toPrimitive(Card... cards) {
		long mask = 0;
		for (Card card : cards) {
			mask |= 1L << card.ordinal();
		}
		return mask;
	}

	/**
	 * @return primitive
	 */
	public long toPrimitive() {
		return mask;
	}

	/**
	 * @return list of Card
	 */
	public Card[] toArray() {
		long mask = this.mask;
		final int size = Long.bitCount(mask);
		final Card[] cards = new Card[size];
		for (int i = 0; i < size; i++) {
			final int ordinal = Long.numberOfTrailingZeros(mask);
			cards[i] = Card.values[ordinal];
			mask ^= 1L << ordinal;
		}
		return cards;
	}

	/**
	 * @return num of card
	 */
	public int size() {
		return Long.bitCount(mask);
	}

	public boolean isSameSuit() {
		return (mask & MASK_SUIT[0]) == mask || (mask & MASK_SUIT[1]) == mask || (mask & MASK_SUIT[2]) == mask || (mask & MASK_SUIT[3]) == mask;
	}

	public int countRed() {
		return Long.bitCount(mask & MASK_RED);
	}

	public int countBlack() {
		return Long.bitCount(mask & MASK_BLACK);
	}

	public boolean isSameColor() {
		final int countRed = Long.bitCount(mask & MASK_RED);
		return countRed == 0 || countRed == 13;
	}

	public int countSuits() {
		int countSuits = 0;
		for (Suit suit : Suit.values) if ((mask & MASK_SUIT[suit.ordinal()]) != 0) countSuits++;
		return countSuits;
	}

	public int countRanks() {
		int countRanks = 0;
		for (Rank rank : Rank.values) if ((mask & MASK_RANK[rank.ordinal()]) != 0) countRanks++;
		return countRanks;
	}

	public int countSuit(Suit suit) {
		return Long.bitCount(mask & MASK_SUIT[suit.ordinal()]);
	}

	public int countRank(Rank rank) {
		return Long.bitCount(mask & MASK_RANK[rank.ordinal()]);
	}

	@Override
	public int hashCode() {
		return Long.hashCode(mask);
	}

	@Override
	public boolean equals(Object object) {
		return object == this || object instanceof Cards && mask == ((Cards) object).mask;
	}

	@Override
	public String toString() {
		if (mask == 0) return "[]";

		final int size = size();
		final StringBuilder builder = new StringBuilder().append('[');
		long mask = this.mask;
		for (int i = 0; i < size; i++) {
			if (i != 0) builder.append(',');
			int ordinal = Long.numberOfTrailingZeros(mask);
			builder.append(Card.values[ordinal]);
			mask ^= 1L << ordinal;
		}
		return builder.append(']').toString();
	}

	@Override
	public int compareTo(Cards cards) {
		return Long.compare(mask, cards.mask);
	}
}
