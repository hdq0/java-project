package mrmathami.chinesepoker.world.game;

import mrmathami.utilities.ThreadFactoryBuilder;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Game {
	private final ExecutorService asyncExecutor = Executors.newCachedThreadPool(
			new ThreadFactoryBuilder()
					.setNamePrefix("GameAsyncExecutor")
					.setDaemon(true)
					.setPriority(Thread.MAX_PRIORITY)
					.build()
	);
	private final ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor(
			new ThreadFactoryBuilder()
					.setNamePrefix("GameScheduledExecutor")
					.setDaemon(true)
					.setPriority(Thread.MAX_PRIORITY)
					.build()
	);
	private final Random randomSource = new SecureRandom();
	private final long duration;
	private final TimeUnit timeUnit;
	private final Players players;

	private Game(long duration, TimeUnit timeUnit, Player[] players) {
		this.duration = duration;
		this.timeUnit = timeUnit;
		this.players = Players.of(players);

	}

	public static Game of(long duration, TimeUnit timeUnit, Player... players) {
		return new Game(duration, timeUnit, players);
	}

	public GameInstance createInstance(Runnable onStartCallback, Runnable onTickCallback, Runnable onFinishCallback) {
		return GameInstance.of(this, onStartCallback, onTickCallback, onFinishCallback);
	}

	public Players getPlayers() {
		return players;
	}

	public long getDuration() {
		return duration;
	}

	public TimeUnit getTimeUnit() {
		return timeUnit;
	}

	ExecutorService getAsyncExecutor() {
		return asyncExecutor;
	}

	ScheduledExecutorService getScheduledExecutor() {
		return scheduledExecutor;
	}

	Random getRandomSource() {
		return randomSource;
	}
}