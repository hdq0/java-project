package mrmathami.chinesepoker.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

import java.net.URL;
import java.util.ResourceBundle;

public final class MainSceneController implements Initializable {
	@FXML public GridPane firstHand;
	@FXML public Label firstScore;
	@FXML public Label firstDeltaScore;
	@FXML public HandSetFragmentController firstHandController;

	@FXML public GridPane secondHand;
	@FXML public Label secondScore;
	@FXML public Label secondDeltaScore;
	@FXML public HandSetFragmentController secondHandController;

	@FXML public GridPane thirdHand;
	@FXML public Label thirdScore;
	@FXML public Label thirdDeltaScore;
	@FXML public HandSetFragmentController thirdHandController;

	@FXML public GridPane mainHand;
	@FXML public Label mainScore;
	@FXML public Label mainDeltaScore;
	@FXML public HandSetFragmentController mainHandController;

	@FXML public Button start;


	private GUIController guiController;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
	}

	public void onStartAction(ActionEvent event) {
		guiController.onStartAction();
	}

	public GUIController getGuiController() {
		return guiController;
	}

	public void setGuiController(GUIController guiController) {
		this.guiController = guiController;
	}
}


