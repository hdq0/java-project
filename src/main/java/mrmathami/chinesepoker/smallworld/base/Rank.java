package mrmathami.chinesepoker.smallworld.base;

public final class Rank {
	public static final int INVALID = -1;
	// region
	public static final int RANK_2 = 0;
	public static final int RANK_3 = 1;
	public static final int RANK_4 = 2;
	public static final int RANK_5 = 3;
	public static final int RANK_6 = 4;
	public static final int RANK_7 = 5;
	public static final int RANK_8 = 6;
	public static final int RANK_9 = 7;
	public static final int RANK_10 = 8;
	public static final int RANK_J = 9;
	public static final int RANK_Q = 10;
	public static final int RANK_K = 11;
	public static final int RANK_A = 12;
	// endregion
	public static final int LENGTH = 13;

	private static final int[] values = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
	private static final String[] RANK_NAME_TABLE = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

	private Rank() {
	}

	public static int[] values() {
		return values.clone();
	}

	public static boolean isValid(int rankNum) {
		return rankNum >= 0 && rankNum < LENGTH;
	}

	public static String toString(int rankNum) {
		// should be an assertion, this function is internal use only
		assert isValid(rankNum) : "rankNum out of range! rankNum = " + rankNum;
		return RANK_NAME_TABLE[rankNum];
	}
}
