package mrmathami.chinesepoker.world.game;

import mrmathami.chinesepoker.world.base.HandSet;
import mrmathami.utilities.Timer;

import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public final class PlayerInstance {
	private final Game game;
	private final Player player;
	private final TableHands tableHands;
	private final PlayerOrder playerOrder;
	private Future<?> future;
	private HandSet result;
	private Timer timer;

	private PlayerInstance(Game game, Player player, TableHands tableHands, PlayerOrder playerOrder) {
		this.game = game;
		this.player = player;
		this.tableHands = tableHands;
		this.playerOrder = playerOrder;
	}

	public static PlayerInstance of(Game game, Player player, TableHands tableHands, PlayerOrder playerOrder) {
		return new PlayerInstance(game, player, tableHands, playerOrder);
	}

	// world
	public synchronized final boolean isStarted() {
		return future != null;
	}

	public synchronized final boolean isFinished() {
		return future != null && future.isDone();
	}

	public synchronized final HandSet getResult() {
		return result;
	}

	public synchronized final void start() {
		if (future != null) return;
		this.result = null;
		this.future = game.getAsyncExecutor().submit(this::run);
	}

	public synchronized void stop() {
		if (future != null) future.cancel(true);
	}

	// ==========

	private void run() {
		final long duration = game.getDuration();
		final TimeUnit timeUnit = game.getTimeUnit();
		this.timer = Timer.of(duration, timeUnit);
		this.result = HandSet.EMPTY;
		final ScheduledFuture<?> scheduledFuture = game.getScheduledExecutor().schedule(this::stop, duration, timeUnit);
		player.getPlayerBrain().play(tableHands, playerOrder, timer, this::set);
		scheduledFuture.cancel(false);
	}

	private synchronized void set(HandSet result) throws InterruptedException {
		if (timer.isExpired()) throw new InterruptedException();
		this.result = result;
	}
}
