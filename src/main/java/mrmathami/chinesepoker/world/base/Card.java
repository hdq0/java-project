package mrmathami.chinesepoker.world.base;

public enum Card {
	CARD_2C("2♣"), CARD_2D("2♦"), CARD_2H("2♥"), CARD_2S("2♠"),
	CARD_3C("3♣"), CARD_3D("3♦"), CARD_3H("3♥"), CARD_3S("3♠"),
	CARD_4C("4♣"), CARD_4D("4♦"), CARD_4H("4♥"), CARD_4S("4♠"),
	CARD_5C("5♣"), CARD_5D("5♦"), CARD_5H("5♥"), CARD_5S("5♠"),
	CARD_6C("6♣"), CARD_6D("6♦"), CARD_6H("6♥"), CARD_6S("6♠"),
	CARD_7C("7♣"), CARD_7D("7♦"), CARD_7H("7♥"), CARD_7S("7♠"),
	CARD_8C("8♣"), CARD_8D("8♦"), CARD_8H("8♥"), CARD_8S("8♠"),
	CARD_9C("9♣"), CARD_9D("9♦"), CARD_9H("9♥"), CARD_9S("9♠"),
	CARD_10C("10♣"), CARD_10D("10♦"), CARD_10H("10♥"), CARD_10S("10♠"),
	CARD_JC("J♣"), CARD_JD("J♦"), CARD_JH("J♥"), CARD_JS("J♠"),
	CARD_QC("Q♣"), CARD_QD("Q♦"), CARD_QH("Q♥"), CARD_QS("Q♠"),
	CARD_KC("K♣"), CARD_KD("K♦"), CARD_KH("K♥"), CARD_KS("K♠"),
	CARD_AC("A♣"), CARD_AD("A♦"), CARD_AH("A♥"), CARD_AS("A♠");

	public static final Card[] values = Card.values();
	private final String string;

	Card(String string) {
		this.string = string;
	}

	@Override
	public String toString() {
		return this.string;
	}

	public Rank getRank() {
		return Rank.values[getRankOrdinal()];
	}

	public int getRankOrdinal() {
		return ordinal() >> 2;
	}

	public Suit getSuit() {
		return Suit.values[getSuitOrdinal()];
	}

	public int getSuitOrdinal() {
		return ordinal() & 3;
	}

	public boolean isRed() {
		return getSuit().isRed();
	}

	public boolean isBlack() {
		return getSuit().isBlack();
	}
}
