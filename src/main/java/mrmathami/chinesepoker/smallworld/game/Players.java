package mrmathami.chinesepoker.smallworld.game;

import mrmathami.utilities.enumerate.EnumArray;
import mrmathami.utilities.enumerate.ReadOnlyEnumArray;

public final class Players extends ReadOnlyEnumArray<PlayerOrder, Player> {
	private Players(EnumArray<PlayerOrder, Player> enumArray) {
		super(enumArray);
	}

	public static Players of(Player... players) {
		assert players.length == PlayerOrder.values.length;
		final EnumArray<PlayerOrder, Player> enumArray = new EnumArray<>(PlayerOrder.class, Player.class);
		for (final PlayerOrder playerOrder : PlayerOrder.values) {
			enumArray.put(playerOrder, players[playerOrder.ordinal()]);
		}
		return new Players(enumArray);
	}
}
